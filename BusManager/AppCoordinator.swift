//
//  AppCoordinator.swift
//  BusManager
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit
import BusManagerCore

public final class AppCoordinator: Coordinator {
    public var navigationController = UINavigationController()
    public var coordinators: [Coordinator] = []
    
    private let window: UIWindow
    private var webService = WebServiceAssembler().webService
    private let storageContext = DBAssembler().storageContext
    
    init(window: UIWindow) {
        self.window = window
    }
    
    public func start() {
        let mapCoordinator = MapCoordinator(webService: webService, storageContext: storageContext, navigationController: navigationController)
        mapCoordinator.start()
        coordinators.append(mapCoordinator)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    
}

