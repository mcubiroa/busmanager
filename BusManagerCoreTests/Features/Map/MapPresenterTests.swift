//
//  MapPresenterTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 20/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
import MapKit
@testable import BusManagerCore

// MARK: - MapViewControllerMock
fileprivate class MapViewControllerMock: MapPresenterToViewProtocol {
    var showTripsCalled = false
    var clearMapCalled = false
    var centerRouteCalled = false
    var showPolylineCalled = false
    var showAnnotationCalled = false
    var showOriginCalled = false
    var showErrorCalled = false
    var showSpinnerCalled = false
    var hideSpinnerCalled = false
    var updateCalled = false
    
    func show(trips: [Trip]) {
        showTripsCalled = true
    }
    
    func showAnnotation(type: StopAnnotationType, point: Point, title: String, subtitle: String?) {
        showAnnotationCalled = true
    }
    
    func clearMap() {
        clearMapCalled = true
    }
    
    func showSpinner() {
        showAnnotationCalled = true
    }
    
    func hideSpinner() {
        hideSpinnerCalled = true
    }
    
    func update(trip: Trip, atIndex index: Int) {
        updateCalled = true
    }
    
    func centerRoute(for coordinates: [CLLocationCoordinate2D]) {
        centerRouteCalled = true
    }
    
    func showPolyline(coordinates: [CLLocationCoordinate2D]) {
        showPolylineCalled = true
    }
    
    func showError(error: ServiceError) {
        showErrorCalled = false
    }
}

// MARK: - MapInteractorMock
fileprivate class MapInteractorMock: MapPresenterToInteractorProtocol {
    var fetchTripsCalled = false
    var fetchStopsCalled = false
    
    func fetchTrips(completion: @escaping (Result<[Trip], ServiceError>) -> Void) {
        fetchTripsCalled = true
        completion(.success([]))
    }
    
    func fetchStops(of trip: Trip, completion: @escaping (Result<Trip, ServiceError>) -> Void) {
        fetchStopsCalled = true
        completion(.success(Trip(tripResponse:
        TripResponse(status: "ongoing", origin: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), stops: [StopResponse(point: PointResponse(latitude: 0, longitude: 0), id: 0)], destination: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), endTime: "", startTime: "", tripDescription: "", driverName: "", route: ""))))
    }
}

// MARK: - MapCoordinatorMock
fileprivate class MapCoordinatorMock: MapCoordinatorProtocol {
    var addBugCalled = false
    
    func addBug() {
        addBugCalled = true
    }
}

// MARK: - MapPresenterTests
class MapPresenterTests: XCTestCase {
    
    var mapPresenter: MapPresenter!
    fileprivate var mapViewMock: MapViewControllerMock!
    fileprivate var mapInteractorMock: MapInteractorMock!
    fileprivate var mapCoordinatorMock: MapCoordinatorMock!

    override func setUp() {
        super.setUp()
        mapViewMock = MapViewControllerMock()
        mapInteractorMock = MapInteractorMock()
        mapCoordinatorMock = MapCoordinatorMock()
        
        mapPresenter = MapPresenter(interactor: mapInteractorMock, coordinator: mapCoordinatorMock)
        mapPresenter.view = mapViewMock
    }


    func testUpdateView() {
        mapPresenter.updateView()
        XCTAssertTrue(mapInteractorMock.fetchTripsCalled)
    }

    func testNewTripSelected() {
        // Given
        let tripMock = Trip(tripResponse:
            TripResponse(status: "ongoing", origin: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), stops: [StopResponse(point: PointResponse(latitude: 0, longitude: 0), id: 0)], destination: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), endTime: "", startTime: "", tripDescription: "", driverName: "", route: ""))
        // When
        mapPresenter.tripSelected(trip: tripMock, atIndex: 0)
        
        // Then
        XCTAssertTrue(mapInteractorMock.fetchStopsCalled)
        XCTAssertTrue(mapViewMock.clearMapCalled)
        XCTAssertTrue(mapViewMock.hideSpinnerCalled)
        XCTAssertTrue(mapViewMock.updateCalled)
        XCTAssertTrue(mapViewMock.showPolylineCalled)
        XCTAssertTrue(mapViewMock.centerRouteCalled)
    }
    
    func testOldTripSelected() {
        // Given
        let tripMock = Trip(tripResponse:
            TripResponse(status: "ongoing", origin: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), stops: [], destination: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), endTime: "", startTime: "", tripDescription: "", driverName: "", route: ""))
        // When
        mapPresenter.tripSelected(trip: tripMock, atIndex: 0)
        
        // Then
        XCTAssertFalse(mapInteractorMock.fetchStopsCalled)
        XCTAssertTrue(mapViewMock.clearMapCalled)
        XCTAssertTrue(mapViewMock.updateCalled)
        XCTAssertTrue(mapViewMock.showPolylineCalled)
        XCTAssertTrue(mapViewMock.centerRouteCalled)
    }
    
    func testHandleAction() {
        mapPresenter.handleAddAction()
        
        XCTAssertTrue(mapCoordinatorMock.addBugCalled)
    }

}
