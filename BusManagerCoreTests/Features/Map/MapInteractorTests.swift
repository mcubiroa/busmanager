//
//  MapInteractorTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 21/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import BusManagerCore

class MapInteractorTests: XCTestCase {
    
    var session: URLSessionMock!
    var webService : WebService!
    var mapInteractor: MapInteractor!
    let testURL = URL(string: "www.tets.com")!
    let timeout = 0.5
    let tripResponse = TripResponse(
        status: "ongoing",
        origin: DestinationResponse(address: "Diagonal 1", point: PointResponse(latitude: 1, longitude: 1)),
        stops: [StopResponse(point: PointResponse(latitude: 2, longitude: 2), id: 0)],
        destination: DestinationResponse(address: "Diagonal 2", point: PointResponse(latitude: 1, longitude: 2)),
        endTime: "2018-12-18T09:00:00.000Z",
        startTime: "2018-12-18T08:00:00.000Z",
        tripDescription: "Barcelona a Martorell",
        driverName: "Alberto Morales",
        route: "")
    let stopDetailResponse = StopDetailResponse(stopTime: "0", paid: true, address: "Diagonal 2", tripId: 1, userName: "Manolo", point: PointResponse(latitude: 0, longitude: 0), price: 11.1)
    

    override func setUp() {
        super.setUp()
        
        session = URLSessionMock()
        webService = WebService(session: session)
        mapInteractor = MapInteractor(webService: webService)
    }

    func testFetchTripsSuccess() {
        // Given
        let exp = expectation(description: "Fetching trips")
        var trips: [Trip] = []
        let expectedTrip = Trip(tripResponse: tripResponse)
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = [tripResponse].toData()
        
        var success = false
        // When
        mapInteractor.fetchTrips { (result) in
            switch result {
            case .success(let tripsResult):
                success = true
                trips = tripsResult
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        // Then
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, true)
        XCTAssertEqual(trips, [expectedTrip])
    }
    
    func testFetchTripsFailure() {
        // Given
        let exp = expectation(description: "Fetching trips failure")
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 400, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = [tripResponse].toData()
        
        var success = false
        // When
        mapInteractor.fetchTrips { (result) in
            switch result {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        // Then
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, false)
    }
    
    func testFetchStopsSuccess() {
        // Given
        let exp = expectation(description: "Fetching stop detail")
        var trip: Trip?
        var expectedTrip = Trip(tripResponse: tripResponse)
        expectedTrip.stops[0].detail = StopDetail(id: 0, stopDetailResponse: stopDetailResponse)
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = stopDetailResponse.toData()
        
        var success = false
        // When
        mapInteractor.fetchStops(of: Trip(tripResponse: tripResponse)) { (result) in
            switch result {
            case .success(let tripsResult):
                success = true
                trip = tripsResult
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        // Then
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, true)
        XCTAssertEqual(trip, expectedTrip)
    }
    
    func testFetchStopsFailure() {
        // Given
        let exp = expectation(description: "Fetching stop detail failure")
        let responseMock = HTTPURLResponse(url: testURL, statusCode: 400, httpVersion: nil, headerFields: nil)
        session.response = responseMock
        session.data = stopDetailResponse.toData()
        
        var success = false
        // When
        mapInteractor.fetchStops(of: Trip(tripResponse: tripResponse)) { (result) in
            switch result {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        // Then
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(success, false)
    }

}
