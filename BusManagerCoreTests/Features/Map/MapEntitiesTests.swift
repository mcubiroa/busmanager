//
//  MapEntitiesTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 21/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
import Polyline
@testable import BusManagerCore

class MapEntitiesTests: XCTestCase {
    
    var trip: Trip!
    

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testInitFromResponse() {
        // Given
        let originDestination = DestinationResponse(address: "Diagonal 1", point: PointResponse(latitude: 1, longitude: 1))
        let stopResponse = StopResponse(point: PointResponse(latitude: 2, longitude: 2), id: 0)
        let destination = DestinationResponse(address: "Diagonal 2", point: PointResponse(latitude: 3, longitude: 3))
        let tripResponse = TripResponse(
            status: "ongoing",
            origin: originDestination,
            stops: [stopResponse],
            destination: destination,
            endTime: "2018-12-18T09:00:00.000Z",
            startTime: "2018-12-18T08:00:00.000Z",
            tripDescription: "Barcelona a Martorell",
            driverName: "Alberto Morales",
            route: "")
        
        // When
        trip = Trip(tripResponse: tripResponse)
        
        // Then
        XCTAssertEqual(trip.status, TripStatus.ongoing)
        XCTAssertEqual(trip.origin, Destination(destinationResponse: originDestination))
        XCTAssertEqual(trip.stops, [Stop(stopResponse: stopResponse)])
        XCTAssertEqual(trip.destination, Destination(destinationResponse: destination))
        XCTAssertEqual(trip.endTime, "2018-12-18T09:00:00.000Z")
        XCTAssertEqual(trip.startTime, "2018-12-18T08:00:00.000Z")
        XCTAssertEqual(trip.driverName, "Alberto Morales")
        XCTAssertEqual(trip.description, "Barcelona a Martorell")
        XCTAssertEqual(trip.coordinates, [])
    }
    
    func testDestinationInitFromResponse() {
        // Given
        let destinationResponse = DestinationResponse(address: "Diagonal 2", point: PointResponse(latitude: 12, longitude: 123))
        
        // When
        let destination = Destination(destinationResponse: destinationResponse)
        
        // Then
        XCTAssertEqual(destination.address, "Diagonal 2")
        XCTAssertEqual(destination.point, Point(pointResponse: PointResponse(latitude: 12, longitude: 123)))
    }
    
    func testPointInitFromResponse() {
        // Given
        let pointResponse = PointResponse(latitude: 12.1, longitude: 13.1)
        
        // When
        let point = Point(pointResponse: pointResponse)
        
        // Then
        XCTAssertEqual(point.latitude, 12.1)
        XCTAssertEqual(point.longitude, 13.1)
    }
    
    func testStopInitFromResponse() {
        // Given
        let pointResponse = PointResponse(latitude: 12.1, longitude: 13.1)
        let stopResponse = StopResponse(point: pointResponse, id: 0)
        
        // When
        let stop = Stop(stopResponse: stopResponse)
        
        // Then
        XCTAssertEqual(stop?.id, 0)
        XCTAssertEqual(stop?.point, Point(pointResponse: pointResponse))
    }
    
    func testStopNilInitFromResponse() {
        // Given
        let pointResponse = PointResponse(latitude: 12.1, longitude: 13.1)
        let stopResponse = StopResponse(point: pointResponse, id: nil)
        
        // When
        let stop = Stop(stopResponse: stopResponse)
        
        // Then
        XCTAssertNil(stop)
    }

}
