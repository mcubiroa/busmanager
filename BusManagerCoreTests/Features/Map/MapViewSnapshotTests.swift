//
//  MapViewSnapshotTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
import SnapshotTesting
@testable import BusManagerCore

class MapPresenterMock: MapViewToPresenterProtocol {    
    var view: MapViewController!
    private let tripResponse = TripResponse(status: "ongoing", origin: DestinationResponse(address: "Av Diagonal 305", point: PointResponse(latitude: 0, longitude: 0)), stops: [StopResponse(point: PointResponse(latitude: 0, longitude: 0), id: 0)], destination: DestinationResponse(address: "", point: PointResponse(latitude: 0, longitude: 0)), endTime: "", startTime: "", tripDescription: "Barcelona - Martorell", driverName: "Pedro", route: "")
    
    func updateView() {
        view?.show(trips: [Trip(tripResponse: tripResponse), Trip(tripResponse: tripResponse)])
    }
    
    func tripSelected(trip: Trip, atIndex index: Int) {
    }
    
    func handleAddAction() {
    }
}

class MapViewSnapshotTests: XCTestCase {
    
    var presenter: MapPresenterMock!
    var vc: MapViewController!

    override func setUp() {
        presenter = MapPresenterMock()
        vc = MapViewController(presenter: presenter)
        presenter.view = vc
    }

    func testLightMode() {
        vc.overrideUserInterfaceStyle = .light
        
        assertSnapshots(matching: vc, as: [
            .image(on: .iPhoneSe),
            .image(on: .iPhoneX),
        ])
    }
    
    func testDarkMode() {
        vc.overrideUserInterfaceStyle = .dark
        
        assertSnapshots(matching: vc, as: [
            .image(on: .iPhone8),
            .image(on: .iPhoneXsMax)
        ])
    }

}
