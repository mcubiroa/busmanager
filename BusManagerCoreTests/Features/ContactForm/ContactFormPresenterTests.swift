//
//  ContactFormPresenterTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import BusManagerCore

// MARK: - ContactFormViewControllerMock
fileprivate class ContactFormViewControllerMock: ContactFormPresenterToViewProtocol {
    var showErrorCalled = false
    var error = ""
    
    func showError(error: String) {
        showErrorCalled = true
        self.error = error
    }
}

// MARK: - ContactFormCoordinatorMock
fileprivate class ContactFormInteractorMock: ContactFormPresenterToInteractorProtocol {
    var storeIssueCalled = false
    
    func storeIssue(issue: Issue, completion: @escaping (Error?) -> Void) {
        storeIssueCalled = true
        if issue.name == "a" {
            completion(nil)
        } else {
            completion(NSError(domain: "failed", code: 01, userInfo: nil))
        }
        
    }
}

// MARK: - ContactFormCoordinatorMock
fileprivate class ContactFormCoordinatorMock: ContactFormCoordinatorProtocol {
    var addIssueCalled = false
    
    func issueAdded() {
        addIssueCalled = true
    }
}


// MARK: - ContactFormPresenterTests
class ContactFormPresenterTests: XCTestCase {

    var contactFormPresenter: ContactFormPresenter!
    fileprivate var contactFormViewMock: ContactFormViewControllerMock!
    fileprivate var contactFormInteractorMock: ContactFormInteractorMock!
    fileprivate var contactFormCoordinatorMock: ContactFormCoordinatorMock!
    
    override func setUp() {
        super.setUp()
        contactFormViewMock = ContactFormViewControllerMock()
        contactFormInteractorMock = ContactFormInteractorMock()
        contactFormCoordinatorMock = ContactFormCoordinatorMock()
        
        contactFormPresenter = ContactFormPresenter(interactor: contactFormInteractorMock, coordinator: contactFormCoordinatorMock)
        contactFormPresenter.view = contactFormViewMock
    }

    func testSaveFieldsSuccess() {
        // When
        contactFormPresenter.saveFields(name: "a", surname: "a", email: "aa@aa.com", phone: "", issueDescription: "abc")
        // Then
        XCTAssertTrue(contactFormInteractorMock.storeIssueCalled)
        XCTAssertTrue(contactFormCoordinatorMock.addIssueCalled)
    }
    
    func testSaveFieldsFailure() {
        // When
        contactFormPresenter.saveFields(name: "ab", surname: "a", email: "aa@aa.com", phone: "", issueDescription: "abc")
        // Then
        XCTAssertTrue(contactFormInteractorMock.storeIssueCalled)
        XCTAssertFalse(contactFormCoordinatorMock.addIssueCalled)
        XCTAssertTrue(contactFormViewMock.showErrorCalled)
    }

    func testValidateEmptyFields() {
        // When
        _ = contactFormPresenter.validate(name: "", surname: "", email: "", phone: "", issueDescription: "")
        // Then
        XCTAssertTrue(contactFormViewMock.showErrorCalled)
    }
    
    func testValidateWrongPhoneFields() {
        // When
        _ = contactFormPresenter.validate(name: "a", surname: "a", email: "aa@aa.com", phone: "656", issueDescription: "abc")
        // Then
        XCTAssertTrue(contactFormViewMock.showErrorCalled)
        XCTAssertEqual(contactFormViewMock.error, "core_validatorError_phone".localized)
    }
    
    func testValidateWrongEmailFields() {
        // When
        _ = contactFormPresenter.validate(name: "a", surname: "a", email: "aaaa.com", phone: "656", issueDescription: "abc")
        // Then
        XCTAssertTrue(contactFormViewMock.showErrorCalled)
        XCTAssertEqual(contactFormViewMock.error, "core_validatorError_email".localized)
    }
    
    func testValidateWrongLenghtFields() {
        // When
        _ = contactFormPresenter.validate(name: "a", surname: "a", email: "aaaa.com", phone: "656", issueDescription: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
        // Then
        XCTAssertTrue(contactFormViewMock.showErrorCalled)
        XCTAssertEqual(contactFormViewMock.error, "core_contact_form_long_issue_error".localized)
    }
}
