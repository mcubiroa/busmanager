//
//  ContactFormViewSnapshotTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
import SnapshotTesting
@testable import BusManagerCore

class ContactFormPresenterMock: ContactFormViewToPresenterProtocol {
    func saveFields(name: String, surname: String, email: String, phone: String, issueDescription: String) {
        
    }
}

class ContactFormViewSnapshotTests: XCTestCase {

    var vc: ContactFormViewController!

    override func setUp() {
        vc = ContactFormViewController(presenter: ContactFormPresenterMock())
    }

    func testLightMode() {
        vc.overrideUserInterfaceStyle = .light
        
        assertSnapshots(matching: vc, as: [
            .image(on: .iPhoneSe),
            .image(on: .iPhoneX),
        ])
    }
    
    func testDarkMode() {
        vc.overrideUserInterfaceStyle = .dark
        
        assertSnapshots(matching: vc, as: [
            .image(on: .iPhone8),
            .image(on: .iPhoneXsMax)
        ])
    }

}
