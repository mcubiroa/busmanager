//
//  ContactFormInteractorTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
import CoreData
@testable import BusManagerCore

// MARK: - ContactFormInteractorTests
class ContactFormInteractorTests: XCTestCase {
    
    var session: URLSessionMock!
    var webService : WebService!
    var storageContext: CoreDataStorageContext!
    var contactFormInteractor: ContactFormInteractor!

    override func setUp() {
        session = URLSessionMock()
        webService = WebService(session: session)
        storageContext = CoreDataStorageContext(configuration: .inMemory(identifier: "BusManagerTests"))
        contactFormInteractor = ContactFormInteractor(webService: webService, storageContext: storageContext)
    }

    func testStoreIssueSuccess() {
        // Given
        let issue = Issue(name: "a", surname: "a", date: Date(), email: "aa@gmail.com", phone: "", issueDescription: "abc")
        // When
        var success = false
        contactFormInteractor.storeIssue(issue: issue) { (error) in
            if error == nil {
                success = true
            }
        }
        XCTAssertTrue(success)
    }

}
