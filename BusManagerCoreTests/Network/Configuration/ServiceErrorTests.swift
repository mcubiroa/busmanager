//
//  ServiceErrorTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 20/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import BusManagerCore

class ServiceErrorTests: XCTestCase {
    
    var error: NSError!
    
    func testUnexpected() {
        error = NSError(domain: "", code: 0)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .unexpected)
        XCTAssertEqual(serviceError.description, "Unexpected error")
    }
    
    func testNoNetwork() {
        error = NSError(domain: "", code: 001)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .noNetwork)
        XCTAssertEqual(serviceError.description, "No internet connection")
    }
    
    func testUnauthorized() {
        error = NSError(domain: "", code: 401, userInfo: nil)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .unauthorized)
        XCTAssertEqual(serviceError.description, "Unauthorized")
    }
    
    func testInternalServer() {
        error = NSError(domain: "", code: 500, userInfo: nil)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .internalServer)
        XCTAssertEqual(serviceError.description, "Server error")
    }

    func testTimedOut() {
        error = NSError(domain: "", code: -1001, userInfo: nil)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .timedOut)
        XCTAssertEqual(serviceError.description, "Timed out")
    }
    
    func testNoContent() {
        error = NSError(domain: "", code: -1014, userInfo: nil)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .noContent)
        XCTAssertEqual(serviceError.description, "No content")
    }
    
    func testUnknown() {
        error = NSError(domain: "", code: 2, userInfo: nil)
        let serviceError = ServiceError.mapServiceError(error: error)
        
        XCTAssertEqual(serviceError, .unknown(error: error))
        XCTAssertEqual(serviceError.description, "The operation couldn’t be completed. ( error 2.)")
    }
}
