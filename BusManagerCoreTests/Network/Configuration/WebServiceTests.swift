//
//  WebServiceTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 20/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import BusManagerCore

class URLDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void

    init(closure: @escaping () -> Void) {
        self.closure = closure
    }
    override func resume() {
        closure()
    }
}

class URLSessionMock: URLSession {
    var data: Data?
    var response: URLResponse?
    var error: Error?
        
    override init() {
    }
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let data = self.data
        let response = self.response
        let error = self.error
        
        return URLDataTaskMock {
             completionHandler(data, response, error)
        }
    }
}

struct MockResponse: Codable {
    var test: String
}

class WebServiceTests: XCTestCase {
    
    let timeout = 0.5
    let testURL = URL(string: "www.test.com")!
    
    var urlSessionMock: URLSessionMock!
    var webService: WebService!

    override func setUp() {
        super.setUp()
        
        urlSessionMock = URLSessionMock()
        webService = WebService(session: urlSessionMock)
    }

    func testLoadSuccess() {
        let exp = expectation(description: "Fetching...")
        var success: Bool = false
        
        let expectedResponse = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        urlSessionMock.response = expectedResponse
        urlSessionMock.data = MockResponse(test: "test").toData()
        
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                success = true
            case .failure:
                success = false
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(true, success)
    }
    
    func testLoadDecodeFailure() {
        let exp = expectation(description: "Fetching...")
        var serviceError: ServiceError?
        
        let expectedResponse = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        urlSessionMock.response = expectedResponse
        urlSessionMock.data = Data()
        
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                serviceError = nil
            case .failure(let error):
                serviceError = error
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(serviceError, .decodeError)
    }
    
    func testLoadNoContentFailure() {
        let exp = expectation(description: "Fetching...")
        var serviceError: ServiceError?
        
        let expectedResponse = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        urlSessionMock.response = expectedResponse
        
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                serviceError = nil
            case .failure(let error):
                serviceError = error
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(serviceError, .noContent)
    }
    
    func testLoadInternalServerFailure() {
        let exp = expectation(description: "Fetching...")
        var serviceError: ServiceError?
        
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                serviceError = nil
            case .failure(let error):
                serviceError = error
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(serviceError, .internalServer)
    }

    func testLoadUnexpectedFailure() {
        let exp = expectation(description: "Fetching...")
        var serviceError: ServiceError?
        
        let expectedResponse = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        urlSessionMock.response = expectedResponse
        urlSessionMock.error = NSError(domain: "", code: 0, userInfo: nil)
           
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                serviceError = nil
            case .failure(let error):
                serviceError = error
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(serviceError, .unexpected)
    }
    
    func testLoadNoNetworkFailure() {
        let exp = expectation(description: "Fetching...")
        var serviceError: ServiceError?
        
        let expectedResponse = HTTPURLResponse(url: testURL, statusCode: 200, httpVersion: nil, headerFields: nil)
        urlSessionMock.response = expectedResponse
        urlSessionMock.error = NSError(domain: "", code: 1, userInfo: nil)
           
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                serviceError = nil
            case .failure(let error):
                serviceError = error
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(serviceError, .noNetwork)
    }
    
    func testLoadTimedOutFailure() {
        let exp = expectation(description: "Fetching...")
        var serviceError: ServiceError?
        
        let expectedResponse = HTTPURLResponse(url: testURL, statusCode: 401, httpVersion: nil, headerFields: nil)
        urlSessionMock.response = expectedResponse
        urlSessionMock.data = MockResponse(test: "test").toData()
        
        webService.load(MockResponse.self, endpoint: .trips) { (response) in
            switch response {
            case .success:
                serviceError = nil
            case .failure(let error):
                serviceError = error
            }
            exp.fulfill()
        }
        waitForExpectations(timeout: timeout)
        XCTAssertEqual(serviceError, .unauthorized)
    }

}
