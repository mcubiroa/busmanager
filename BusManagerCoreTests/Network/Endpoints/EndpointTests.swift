//
//  EndpointTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 20/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import BusManagerCore

class EndpointTests: XCTestCase {

    var mockURL = URL(string: "www.test.com/")!

    func testTripsRequest() {
        let endpoint = Endpoint.trips
        let request = endpoint.request(with: mockURL)
        
        XCTAssertEqual(request.url, URL(string: "www.test.com/" + endpoint.path))
        XCTAssertEqual(request.httpMethod, endpoint.httpMehtod.rawValue)
    }
    
    func testStopsRequest() {
        let endpoint = Endpoint.stops(stopId: 0)
        let request = endpoint.request(with: mockURL)
        
        XCTAssertEqual(request.url, URL(string: "www.test.com/" + endpoint.path))
        XCTAssertEqual(request.httpMethod, endpoint.httpMehtod.rawValue)
    }
}
