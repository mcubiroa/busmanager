//
//  DateManagerTests.swift
//  BusManagerCoreTests
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest
@testable import BusManagerCore

class DateManagerTests: XCTestCase {
    
    private var date: Date!

    override func setUp() {
        let calendar = Calendar.init(identifier: .gregorian)
        var components = DateComponents()
        components.year = 2018
        components.month = 12
        components.day = 18
        date = calendar.date(from: components)
    }

    func testGetDateFromISOString() {
        let date = DateManager.date(from: "2018-12-18T00:00:00.000+0100", withFormat: .iso8601)
        XCTAssertEqual(date, self.date)
    }
    
    func testGetDateFrom_ddMMyyy_String() {
        let date = DateManager.date(from: "18/12/2018", withFormat: .ddMMyyyy)
        XCTAssertEqual(date, self.date)
    }
    
    func testGetStringFromISODate() {
        let string = DateManager.string(from: date, format: .iso8601)
        XCTAssertEqual(string, "2018-12-18T00:00:00.000+0100")
    }
    
    func testGetStringFrom_ddMMyyyy_Date() {
        let string = DateManager.string(from: date, format: .ddMMyyyy)
        XCTAssertEqual(string, "18/12/2018")
    }
}
