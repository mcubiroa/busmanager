# Application
 
BusManager is a simple trip manager for your bus on demand solution. You can see all the different available routes. When you click on one trip, it will be shown on the map with all the available stops. If there is a problem with one or more stops an error will appear. You can click on each stop to see the details. If you see an issue on the application, open the contact form and report it by touching the plus button.

### Map View

![picture](Screenshots/map.png)

### Contact Form View

![picture](Screenshots/form.png)

### Installation instructions 

To run the app in your computer follow these steps:

1. cd to repository
2. pod install (version of cocoapod used 1.6.1)

### Project structure and Architecture

BusManager project is structured in different modules. This structure helps to avoid conflicts, reuse code between the different parts of the application and allows the use of playgrounds. In addition, you can compile the different part of the application separately. Each feature of the app is coded following VIPER design pattern with modifications. I've removed the Router layer and added an Assebler and Coordinator.

![picture](Screenshots/module_architecture.jpg)

This design pattern isolates each layer and creates a good environment for unit tests.

### Following steps

These are the future steps to improve the application.

- Add test to DB layer
- Create base Coordinator to handle add and remove childs coordinators
- Create intermediate layer to handle table data source outside the view
- Improve view design