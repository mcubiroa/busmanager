//
//  BusManagerUITests.swift
//  BusManagerUITests
//
//  Created by Marc Cubiro Aguilar on 20/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import XCTest

class BusManagerUITests: XCTestCase {

    let timeout: TimeInterval = 10
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        continueAfterFailure = false
    }

    func testLaunchOnMap() {
        app.launch()
    
        XCTAssertTrue(app.otherElements["MapView"].waitForExistence(timeout: timeout))
    }

    func testNavigateToAddIssue() {
        
        app.launch()
        app.navigationBars["Trips"].buttons["Add"].tap()
        
        XCTAssertTrue(app.otherElements["FormIssueView"].waitForExistence(timeout: timeout))
    }
}
