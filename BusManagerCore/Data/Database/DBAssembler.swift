//
//  DBAssembler.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

public final class DBAssembler {
    public var storageContext: CoreDataStorageContext = CoreDataStorageContext()
    
    public init() {}
}
