//
//  CoreDataStorageContext.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation
import CoreData

// MARK: - CoreDataStorageContext
public class CoreDataStorageContext {
    var managedContext: NSManagedObjectContext?

    required init(configuration: ConfigurationType = .basic(identifier: "BusManager")) {
        switch configuration {
        case .basic:
            initDB(modelName: configuration.identifier(), storeType: .sqLiteStoreType)
        case .inMemory:
            initDB(storeType: .inMemoryStoreType)
        }
    }

    private func initDB(modelName: String? = nil, storeType: StoreType) {
        let coordinator = CoreDataStoreCoordinator.persistentStoreCoordinator(modelName: modelName, storeType: storeType)
        self.managedContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        self.managedContext?.persistentStoreCoordinator = coordinator
    }
}

// MARK: - Operations
extension CoreDataStorageContext: StorageContext {
    public func create<DBEntity>(_ model: DBEntity.Type) -> DBEntity? where DBEntity : Storable {
        let entityDescription =  NSEntityDescription.entity(forEntityName: String.init(describing: model.self),
                                                            in: managedContext!)
        let entity = NSManagedObject(entity: entityDescription!,
                                     insertInto: managedContext)
        return entity as? DBEntity
    }
    
    public func save(object: Storable) throws {
        try managedContext?.save()
    }
    
    public func fetch(_ model: Storable.Type, predicate: NSPredicate?) -> [Storable] {
        return []
    }
}
