//
//  Configuration.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation
import CoreData

// MARK: - ConfigurationType
public enum ConfigurationType {
    case basic(identifier: String)
    case inMemory(identifier: String?)

    func identifier() -> String? {
        switch self {
        case .basic(let identifier): return identifier
        case .inMemory(let identifier): return identifier
        }
    }
}

// MARK: - NSManagedObject + Storable
extension NSManagedObject: Storable {
}
