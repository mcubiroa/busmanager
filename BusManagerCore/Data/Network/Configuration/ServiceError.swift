//
//  ServiceError.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

public enum ServiceError: Error, Equatable {
    case decodeError
    case noNetwork
    case timedOut
    case unexpected
    case unauthorized
    case internalServer
    case noContent
    case unknown(error: NSError)
    
    // MARK: - Variables
    public var description: String? {
        switch self {
        case .decodeError:
            return "core_serviceError_decode".localized
        case .unexpected:
            return "core_serviceError_unexpected".localized
        case .noNetwork:
            return "core_serviceError_noNetwork".localized
        case .internalServer:
            return "core_serviceError_internalServer".localized
        case .timedOut:
            return "core_serviceError_timedOut".localized
        case .unauthorized:
            return "core_serviceError_unauthorized".localized
         case .noContent:
            return "core_serviceError_noContent".localized
        case .unknown(let error):
            return error.localizedDescription
        }
    }
    
    // MARK: - Public methods
    static public func mapServiceError(error: NSError) -> ServiceError {
        switch error.code {
        case 000:
            return .unexpected
        case 001, -60, -1009:
            return .noNetwork
        case 401:
            return .unauthorized
        case 500, -1011:
            return .internalServer
        case -1001:
            return .timedOut
        case -1014:
            return .noContent
        default:
            return .unknown(error: error)
        }
    }
}
