//
//  WebService.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - WebService
public class WebService {
    
    // MARK: - Properties
    private let session: URLSession
    private let decoder = JSONDecoder()
    private let baseURL = URL(string: "https://europe-west1-metropolis-fe-test.cloudfunctions.net/api/")!
    
    // MARK: - Variables
    private var dataTask: URLSessionDataTask?
    
    // MARK: - Initialization
    init(session: URLSession) {
        self.session = session
    }
    
    // MARK: - Public methods
    public func load<T>(_ type: T.Type, endpoint: Endpoint, completion: @escaping (Result<T, ServiceError>) -> Void) where T: Decodable {
        let request = endpoint.request(with: baseURL)
        
        dataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                return completion(.failure(ServiceError.mapServiceError(error: error! as NSError)))
            }
            guard let response = response as? HTTPURLResponse else {
                return completion(.failure(.internalServer))
            }
            if 200 ..< 300 ~= response.statusCode {
                if let data = data {
                    if let result = try? self.decoder.decode(T.self, from: data) {
                        completion(.success(result))
                    } else {
                        completion(.failure(.decodeError))
                    }
                } else {
                    completion(.failure(.noContent))
                }
            } else {
                let error = NSError(domain: "", code: response.statusCode)
                completion(.failure(ServiceError.mapServiceError(error: error)))
            }
        }
        
        dataTask?.resume()
    }
}
