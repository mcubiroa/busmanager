//
//  ContactFormAssembler.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - MapAssembler
internal final class ContactFormAssembler {
    private let webService: WebService
    private let storageContext: StorageContext
    private weak var coordinator: ContactFormCoordinatorProtocol?
    
    public init(webService: WebService, storageContext: StorageContext, coordinator: ContactFormCoordinatorProtocol) {
        self.webService = webService
        self.storageContext = storageContext
        self.coordinator = coordinator
    }
    
    public func view() -> UIViewController {
        let contactFormPresenter = presenter()
        let contactFormView = ContactFormViewController(presenter: contactFormPresenter)
        contactFormPresenter.view = contactFormView
        
        return contactFormView
    }
    
    private func presenter() -> ContactFormPresenter {
        let contactFormInteractor = interactor()
        let contactFormPresenter = ContactFormPresenter(interactor: contactFormInteractor, coordinator: coordinator)
        
        return contactFormPresenter
    }
    
    private func interactor() -> ContactFormInteractor {
        return ContactFormInteractor(webService: webService, storageContext: storageContext)
    }
    
}
