//
//  ContactFormPresenter.swift
//  BusManager
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - ContactFormViewToPresenterProtocol
protocol ContactFormViewToPresenterProtocol: AnyObject {    
    func saveFields(name: String, surname: String, email: String, phone: String, issueDescription: String)
}

// MARK: - ContactFormPresenter
class ContactFormPresenter: ContactFormViewToPresenterProtocol {
    
    // MARK: - Variables
    weak var view: ContactFormPresenterToViewProtocol?
    var interactor: ContactFormPresenterToInteractorProtocol?
    private let coordinator: ContactFormCoordinatorProtocol?
    
    // MARK: - Initialization
    init(interactor: ContactFormPresenterToInteractorProtocol, coordinator: ContactFormCoordinatorProtocol?) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
    
    internal func saveFields(name: String, surname: String, email: String, phone: String, issueDescription: String) {
        if validate(name: name, surname: surname, email: email, phone: phone, issueDescription: issueDescription) {
            let issue = Issue(
                name: name,
                surname: surname,
                email: email,
                phone: phone,
                issueDescription: issueDescription)
            
            interactor?.storeIssue(issue: issue, completion: { (error) in
                if error == nil {
                    self.coordinator?.issueAdded()
                } else {
                    self.view?.showError(error: "Unexpected error while saving issue")
                }
            })
        }
    }
    
    internal func validate(name: String, surname: String, email: String, phone: String, issueDescription: String) -> Bool {
        guard name != "", surname != "", email != "", issueDescription != "" else {
            view?.showError(error: "core_contact_form_empty_error".localized)
            return false
        }
        guard issueDescription.count < 200 else {
            view?.showError(error: "core_contact_form_long_issue_error".localized)
            return false
        }
        do {
            _ = try email.validate(forType: .email)
            if phone.count > 0 {
                _ = try phone.validate(forType: .phone)
            }
        } catch let error {
            if let error = error as? ValidationError {
                 view?.showError(error: error.message)
            }
            return false
        }
        return true
    }
}
