//
//  ContactFormEntities.swift
//  BusManager
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//

import UIKit
import CoreData

// MARK: - ContactFormModel
struct Issue {
    var name: String = ""
    var surname: String = ""
    var date: Date = Date()
    var email: String = ""
    var phone: String? = ""
    var issueDescription: String = ""
    
    init(name: String, surname: String, date: Date = Date(), email: String, phone: String?, issueDescription: String) {
        self.name = name
        self.surname = surname
        self.date = date
        self.email = email
        self.phone = phone
        self.issueDescription = issueDescription
    }
    
    init(issueEntity: IssueEntity) {
        self.name = issueEntity.value(forKey: "name") as! String
        self.surname = issueEntity.value(forKey: "surname") as! String
        self.date = issueEntity.value(forKey: "date") as! Date
        self.email = issueEntity.value(forKey: "email") as! String
        self.phone = issueEntity.value(forKey: "phone") as! String?
        self.issueDescription = issueEntity.value(forKey: "issueDescription") as! String
    }
}

// MARK: - Database entities

// MARK: - Issue Entity
class IssueEntity: NSManagedObject {
    func bind(_ issue: Issue) {
        setValue(issue.name, forKey: "userName")
        setValue(issue.surname, forKey: "userSurname")
        setValue(issue.date, forKey: "date")
        setValue(issue.email, forKey: "email")
        setValue(issue.phone, forKey: "phone")
        setValue(issue.issueDescription, forKey: "issueDescription")
    }
}
