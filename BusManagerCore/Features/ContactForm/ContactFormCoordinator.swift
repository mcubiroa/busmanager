//
//  ContactFormCoordinator.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - MapCoordinator
internal protocol ContactFormCoordinatorProtocol: AnyObject {
    func issueAdded() 
}

// MARK: - ContactFormCoordinator
public final class ContactFormCoordinator: Coordinator, ContactFormCoordinatorProtocol {
    public var coordinators = [Coordinator]()
    public var navigationController: UINavigationController
    private let webService: WebService
    private let storageContext: StorageContext
    
    public init(webService: WebService, storageContext: StorageContext, navigationController: UINavigationController) {
        self.webService = webService
        self.storageContext = storageContext
        self.navigationController = navigationController
    }
    
    public func start() {
        let contactFormAssembler = ContactFormAssembler(webService: webService, storageContext: storageContext, coordinator: self)
        navigationController.present(contactFormAssembler.view(), animated: true, completion: nil)
    }
    
    internal func issueAdded() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
