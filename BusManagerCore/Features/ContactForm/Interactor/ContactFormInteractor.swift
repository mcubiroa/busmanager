//
//  ContactFormInteractor.swift
//  BusManager
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

// MARK: - ContactFormPresenterToInteractorProtocol
protocol ContactFormPresenterToInteractorProtocol: AnyObject {
    func storeIssue(issue: Issue, completion: @escaping (Error?) -> Void)
}

// MARK: - ContactFormInteractor
class ContactFormInteractor: ContactFormPresenterToInteractorProtocol{
    internal var webService: WebService
    internal var storageContext: StorageContext

    // MARK: - Initialization
    init(webService: WebService, storageContext: StorageContext) {
        self.webService = webService
        self.storageContext = storageContext
    }
    
    internal func storeIssue(issue: Issue, completion: @escaping (Error?) -> Void) {
        if let issueEntity = storageContext.create(IssueEntity.self) {
            issueEntity.bind(issue)
            do {
                try storageContext.save(object: issueEntity)
            } catch {
                return completion(error)
            }
            increaseBadget()
            return completion(nil)
        }
        return completion(NSError(domain: "Unexpected error", code: 0, userInfo: nil))
    }
    
    private func increaseBadget() {
        BadgeManager.increaseBadget()
    }
}
