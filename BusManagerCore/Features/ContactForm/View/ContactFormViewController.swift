//
//  ContactFormViewController.swift
//  BusManager
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - ContactFormPresenterToViewProtocol
protocol ContactFormPresenterToViewProtocol: AnyObject {
    func showError(error: String)
}

// MARK: - ContactFormViewController
class ContactFormViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var issueLabel: UILabel!
    @IBOutlet weak var issueTextView: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    
    var presenter: ContactFormViewToPresenterProtocol?
    
    // MARK: - Initialization
    init(presenter: ContactFormViewToPresenterProtocol) {
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        self.presenter = presenter
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    // MARK: - Setup
    private func setupUI() {
        titleLabel.text = "core_contact_form_title".localized
        
        setupKeyboard()
        setupLiterals()
        setupIssueTextView()
    }
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tapGesture)
    }
    
    private func setupLiterals() {
        nameLabel.text = "core_contact_form_name".localized
        surnameLabel.text = "core_contact_form_surname".localized
        emailLabel.text = "core_contact_form_email".localized
        phoneLabel.text = "core_contact_form_phone".localized + " " + "core_contact_form_optional".localized
        issueLabel.text = "core_contact_form_issueDescription".localized
    }
    
    private func setupIssueTextView() {
        issueTextView.layer.borderColor = UIColor.lightGray.cgColor
        issueTextView.layer.borderWidth = 1
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)

        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }

    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    // MARK: - IBActions
    @IBAction func saveAction(_ sender: UIButton) {
        presenter?.saveFields(
            name: nameTextField.text ?? "",
            surname: surnameTextField.text ?? "",
            email: emailTextField.text ?? "",
            phone: phoneTextField.text ?? "",
            issueDescription: issueTextView.text)
    }
}

// MARK: - ContactFormPresenterToViewProtocol
extension ContactFormViewController: ContactFormPresenterToViewProtocol {    
    func showError(error: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "core_error_title".localized, message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "core_error_accept".localized, style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
}
