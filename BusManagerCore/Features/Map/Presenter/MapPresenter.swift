//
//  MapPresenter.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - MapViewToPresenterProtocol
internal protocol MapViewToPresenterProtocol: AnyObject {
    func updateView()
    func tripSelected(trip: Trip, atIndex index: Int)
    func handleAddAction()
}

// MARK: - MapPresenter
internal class MapPresenter: MapViewToPresenterProtocol {
    
    // MARK: - Variables
    weak var view: MapPresenterToViewProtocol?
    var interactor: MapPresenterToInteractorProtocol?
    private let coordinator: MapCoordinatorProtocol?
    
    // MARK: - Initialization
    init(interactor: MapPresenterToInteractorProtocol, coordinator: MapCoordinatorProtocol?) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
    
    internal func updateView() {
        view?.showSpinner()
        interactor?.fetchTrips() { result in
            self.view?.hideSpinner()
            switch result {
            case .success(let trips):
                self.view?.show(trips: trips)
            case .failure(let error):
                self.view?.showError(error: error)
            }
        }
    }

    internal func tripSelected(trip: Trip, atIndex index: Int) {
        guard trip.stops.count > 0 && trip.stops[0].detail == nil else {
            return presentRoute(trip: trip, atIndex: index)
        }
        view?.showSpinner()
        interactor?.fetchStops(of: trip, completion: { (result) in
            self.view?.hideSpinner()
            switch result {
            case .success(let updatedTrip):
                self.presentRoute(trip: updatedTrip, atIndex: index)
            case .failure(let serviceError):
                self.view?.showError(error: serviceError)
            }
        })
    }
    
    internal func handleAddAction() {
        coordinator?.addBug()
    }
    
    // MARK: - Private methods
    private func presentRoute(trip: Trip, atIndex index: Int) {
        view?.clearMap()
        
        view?.showAnnotation(type: .origin, point: trip.origin.point, title: "core_trips_origin".localized, subtitle: trip.origin.address)
        view?.showAnnotation(type: .destination, point: trip.destination.point, title: "core_trips_destination".localized, subtitle: trip.destination.address)
        
        showStops(of: trip)
        showPolylines(of: trip)
        
        view?.update(trip: trip, atIndex: index)
        view?.centerRoute(for: trip.coordinates)
    }
    
    private func showStops(of trip: Trip) {
        for stop in trip.stops {
            view?.showAnnotation(type: .stop, point: stop.point, title: stop.detail?.address ?? "-", subtitle:  getStopSubtitle(stopDetail: stop.detail))
        }
    }
    
    private func showPolylines(of trip: Trip) {
        view?.showPolyline(coordinates: trip.coordinates)
    }
    
    private func getStopSubtitle(stopDetail: StopDetail?) -> String {
        return "core_trips_price".localized + ": \(stopDetail?.price ?? 0)€ " + "core_trips_time".localized + ": \(DateManager.string(from: stopDetail?.stopTime ?? Date()))"
    }
}

