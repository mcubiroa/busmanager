//
//  MapCoordinator.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - MapCoordinator
internal protocol MapCoordinatorProtocol {
    func addBug()
}

public final class MapCoordinator: Coordinator, MapCoordinatorProtocol {
    public var coordinators = [Coordinator]()
    public var navigationController: UINavigationController
    private let webService: WebService
    private let storageContext: StorageContext
    
    public init(webService: WebService, storageContext: StorageContext, navigationController: UINavigationController) {
        self.webService = webService
        self.storageContext = storageContext
        self.navigationController = navigationController
    }
    
    public func start() {
        let mapAssembler = MapAssembler(webService: webService, coordinator: self)
        navigationController.pushViewController(mapAssembler.view(), animated: false)
    }
    
    internal func addBug() {
        let contactFormCoordinator = ContactFormCoordinator(webService: webService, storageContext: storageContext, navigationController: navigationController)
        contactFormCoordinator.start()
        coordinators.append(contactFormCoordinator)
    }
}
