//
//  MapAssembler.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

// MARK: - MapAssembler
internal final class MapAssembler {
    private let webService: WebService
    private weak var coordinator: MapCoordinator?
    
    public init(webService: WebService, coordinator: MapCoordinator) {
        self.webService = webService
        self.coordinator = coordinator
    }
    
    public func view() -> UIViewController {
        let mapPresenter = presenter()
        let mapView = MapViewController(presenter: mapPresenter)
        mapPresenter.view = mapView
        
        return mapView
    }
    
    private func presenter() -> MapPresenter {
        let mapInteractor = interactor()
        let mapPresenter = MapPresenter(interactor: mapInteractor, coordinator: coordinator)
        
        return mapPresenter
    }
    
    private func interactor() -> MapInteractor {
        return MapInteractor(webService: webService)
    }
    
}
