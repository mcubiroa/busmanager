//
//  MapInteractor.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//
//

import UIKit

// MARK: - MapPresenterToInteractorProtocol
protocol MapPresenterToInteractorProtocol: AnyObject {
    func fetchTrips(completion: @escaping (Result<[Trip], ServiceError>) -> Void)
    func fetchStops(of trip: Trip, completion: @escaping (Result<Trip, ServiceError>) -> Void)
}

// MARK: - MapInteractor
internal class MapInteractor: MapPresenterToInteractorProtocol{
    
    internal var webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }
    
    // MARK: - MapPresenterToInteractorProtocol
    internal func fetchTrips(completion: @escaping (Result<[Trip], ServiceError>) -> Void) {
        webService.load([TripResponse].self, endpoint: .trips) { (result) in
            switch result {
            case .success(let tripResponse):
                completion(.success(self.getTripsFrom(tripResponse)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    internal func fetchStops(of trip: Trip, completion: @escaping (Result<Trip, ServiceError>) -> Void) {
        guard trip.stops.count > 0 else { return completion(.success(trip)) }
        
        var stopsDetails: [StopDetail] = []
        let dispatchGroup = DispatchGroup()
        
        for stop in trip.stops {
            dispatchGroup.enter()
            webService.load(StopDetailResponse.self, endpoint: .stops(stopId: stop.id)) { (result) in
                switch result {
                case .success(let stopDetailResponse):
                    stopsDetails.append(self.getStopFrom(id: stop.id, stopDetailResponse: stopDetailResponse))
                case .failure(let error):
                    // We can stop the request if some stop fetch fails or we can continue populating all the other stops depending on the bussines logic.
                    return completion(.failure(error))
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: DispatchQueue.main) {
            return completion(.success(self.getUpdated(trip: trip, with: stopsDetails)))
        }
    }
    
    // MARK: - Data transformation methods
    private func getTripsFrom(_ tripsResponse: [TripResponse]) -> [Trip] {
        return tripsResponse.map({ Trip(tripResponse: $0) })
    }
    
    private func getStopFrom(id: Int, stopDetailResponse: StopDetailResponse) -> StopDetail {
        return StopDetail(id: id, stopDetailResponse: stopDetailResponse)
    }
    
    private func getUpdated(trip: Trip, with stopsDetails: [StopDetail]) -> Trip {
        guard trip.stops.count == stopsDetails.count else { return trip }
        
        let sortedStopsById = stopsDetails.sorted(by: { $0.id < $1.id })
        var updatedTrip = trip
        updatedTrip.stops = trip.stops.enumerated().map{ (index, stop) in
            var auxStop = stop
            auxStop.detail = sortedStopsById[index]
            return auxStop
        }
        
        return updatedTrip
    }
}
