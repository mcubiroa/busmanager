//
//  MapEntities.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//

import UIKit
import MapKit
import Polyline

// MARK: Trip Status
internal enum TripStatus: String {
    case ongoing
    case schaduled
    case finalized
    case canceled
    case unknown
}

// MARK: - Trip
internal struct Trip: Equatable {
    var description: String
    var driverName: String
    var status: TripStatus
    let origin: Destination
    var stops: [Stop]
    let destination: Destination
    let endTime: String
    let startTime: String
    let coordinates: [CLLocationCoordinate2D]
    var points: [Point] {
        var tripPoints = [origin.point]
        tripPoints.append(contentsOf: stops.map({ $0.point }))
        tripPoints.append(destination.point)
        
        return tripPoints
    }
    
    init(tripResponse: TripResponse) {
        description = tripResponse.tripDescription
        driverName = tripResponse.driverName
        status = TripStatus(rawValue: tripResponse.status) ?? .unknown
        origin = Destination(destinationResponse: tripResponse.origin)
        stops = tripResponse.stops.compactMap({ Stop(stopResponse: $0) })
        destination = Destination(destinationResponse: tripResponse.destination)
        endTime = tripResponse.endTime
        startTime = tripResponse.startTime
        coordinates = decodePolyline(tripResponse.route) ?? []
    }
}

// MARK: - Destination
struct Destination: Equatable {
    let address: String
    let point: Point
    
    init(destinationResponse: DestinationResponse) {
        address = destinationResponse.address
        point = Point(pointResponse: destinationResponse.point)
    }
}

// MARK: - Point
struct Point: Equatable {
    let latitude: Double
    let longitude: Double
    
    init(pointResponse: PointResponse) {
        latitude = pointResponse.latitude
        longitude = pointResponse.longitude
    }
}

// MARK: - Stop
struct Stop: Equatable {
    let id: Int
    let point: Point
    var detail: StopDetail?
    
    init?(stopResponse: StopResponse) {
        guard let id = stopResponse.id,
            let point = stopResponse.point else { return nil }
        self.id = id
        self.point = Point(pointResponse: point)
    }
}

// MARK: - StopDetail
struct StopDetail: Equatable {
    let id: Int
    let stopTime: Date?
    let paid: Bool
    let address: String
    let tripId: Int
    let userName: String
    let point: Point
    let price: Double
    
    init(id: Int, stopDetailResponse: StopDetailResponse) {
        self.id = id
        self.stopTime = DateManager.date(from: stopDetailResponse.stopTime)
        self.paid = stopDetailResponse.paid
        self.address = stopDetailResponse.address
        self.tripId = stopDetailResponse.tripId
        self.userName = stopDetailResponse.userName
        self.point = Point(pointResponse: stopDetailResponse.point)
        self.price = stopDetailResponse.price
    }
}
