//
//  MapResponseEntities.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - Trip
struct TripResponse: Codable {
    let status: String
    let origin: DestinationResponse
    let stops: [StopResponse]
    let destination: DestinationResponse
    let endTime: String
    let startTime: String
    let tripDescription: String
    let driverName: String
    let route: String

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case origin = "origin"
        case stops = "stops"
        case destination = "destination"
        case endTime = "endTime"
        case startTime = "startTime"
        case tripDescription = "description"
        case driverName = "driverName"
        case route = "route"
    }
}

// MARK: - Destination
struct DestinationResponse: Codable {
    let address: String
    let point: PointResponse

    enum CodingKeys: String, CodingKey {
        case address = "address"
        case point = "point"
    }
}

// MARK: - Point
struct PointResponse: Codable {
    let latitude: Double
    let longitude: Double

    enum CodingKeys: String, CodingKey {
        case latitude = "_latitude"
        case longitude = "_longitude"
    }
}

// MARK: - Stop
struct StopResponse: Codable {
    let point: PointResponse?
    let id: Int?

    enum CodingKeys: String, CodingKey {
        case point = "point"
        case id = "id"
    }
}

// MARK: - StopDetail
struct StopDetailResponse: Codable {
    let stopTime: String
    let paid: Bool
    let address: String
    let tripId: Int
    let userName: String
    let point: PointResponse
    let price: Double

    enum CodingKeys: String, CodingKey {
        case stopTime = "stopTime"
        case paid = "paid"
        case address = "address"
        case tripId = "tripId"
        case userName = "userName"
        case point = "point"
        case price = "price"
    }
}
