//
//  StopAnnotation.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 21/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation
import MapKit

internal enum StopAnnotationType {
    case origin
    case stop
    case destination
}

internal final class StopAnnotation: NSObject, MKAnnotation {
    var type: StopAnnotationType
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(type: StopAnnotationType, coordinate: CLLocationCoordinate2D) {
        self.type = type
        self.coordinate = coordinate
    }
}
