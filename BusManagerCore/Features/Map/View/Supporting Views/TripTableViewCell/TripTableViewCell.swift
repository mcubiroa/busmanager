//
//  TripTableViewCell.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

class TripTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var driverLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        
    }
    
    public func bind(tripName: String, driverName: String, status: TripStatus) {
        titleLabel.text = tripName
        driverLabel.text = driverName
        statusLabel.text = status.rawValue
    }
}
