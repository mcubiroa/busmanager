//
//  MapViewController.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright (c) 2020 marc.cubiro. All rights reserved.
//

import UIKit
import MapKit

// MARK: - MapPresenterToViewProtocol
internal protocol MapPresenterToViewProtocol: AnyObject {
    func show(trips: [Trip])
    func showAnnotation(type: StopAnnotationType, point: Point, title: String, subtitle: String?)
    func clearMap()
    func centerRoute(for coordinates: [CLLocationCoordinate2D])
    func showPolyline(coordinates: [CLLocationCoordinate2D])
    func showError(error: ServiceError)
    func showSpinner()
    func hideSpinner()
    func update(trip: Trip, atIndex index: Int)
}

// MARK: - MapViewController
internal final class MapViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {

    // MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tripsTableView: UITableView!
    
    // MARK: - Properties
    private let kAnnotationIdentifier = "CustomAnnotation"
    private let cellId = String(describing: TripTableViewCell.self)
    
    // MARK: - Variables
    internal var presenter: MapViewToPresenterProtocol?
    private var trips = [Trip]()
    var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Initialization
    init(presenter: MapViewToPresenterProtocol) {
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        self.presenter = presenter
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        presenter?.updateView()
    }
    
    private func setupUI() {
        title = "core_trips_list_title".localized
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        setupTable()
        setupActivityIndicator()
        mapView.delegate = self
    }
    
    private func setupTable() {
        tripsTableView.dataSource = self
        tripsTableView.delegate = self
        tripsTableView.register(
            UINib(nibName: String(describing: TripTableViewCell.self), bundle: Bundle(for: type(of: self))),
            forCellReuseIdentifier: cellId
        )
    }
    
    private func setupActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.hidesWhenStopped = true
        
        let barButton = UIBarButtonItem(customView: activityIndicator)
        navigationItem.setRightBarButton(barButton, animated: true)
    }
    
    @objc private func addTapped() {
        presenter?.handleAddAction()
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let trip = trips[indexPath.row]
        
        if let tripCell = cell as? TripTableViewCell {
            tripCell.bind(tripName: trip.description, driverName: trip.driverName, status: trip.status)
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.tripSelected(trip: trips[indexPath.row], atIndex: indexPath.row)
    }
    
    // MARK: - MKMapViewDelegate
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer()
        }

        let polylineRenderer = MKPolylineRenderer(overlay: polyline)
        polylineRenderer.strokeColor = .red
        polylineRenderer.lineWidth = 2
        return polylineRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let stopAnnotation = annotation as? StopAnnotation else { return nil }
        
        var imageName: String = ""
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: kAnnotationIdentifier)
        annotationView.canShowCallout = true
        
        switch stopAnnotation.type {
        case .origin:
            imageName = "OriginStopIcon"
        case .stop:
            imageName = "BusStopIcon"
        case .destination:
            imageName = "DestinationStopIcon"
        }
        annotationView.image = UIImage(named: imageName, in: Bundle(for: type(of: self)), compatibleWith: nil)
        return annotationView
    }
    
}

extension MapViewController: MapPresenterToViewProtocol {
    func show(trips: [Trip]) {
        self.trips = trips
        
        DispatchQueue.main.async {
            self.tripsTableView.reloadData()
        }
    }
    
    func showAnnotation(type: StopAnnotationType, point: Point, title: String, subtitle: String?) {
        let annotation = StopAnnotation(type: type, coordinate: CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude))
        annotation.title = title
        if let subtitle = subtitle {
            annotation.subtitle = subtitle
        }
        mapView.addAnnotation(annotation)
    }
    
    func clearMap() {
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)
    }
    
    func centerRoute(for coordinates: [CLLocationCoordinate2D]) {
        mapView.fitRoute(for: coordinates)
    }
    
    func showPolyline(coordinates: [CLLocationCoordinate2D]) {
        let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
        mapView.addOverlay(polyline)
    }
    
    func showError(error: ServiceError) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "core_error_title".localized, message: error.description, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "core_error_accept".localized, style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func showSpinner() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func hideSpinner() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func update(trip: Trip, atIndex index: Int) {
        trips[index] = trip
    }
}
