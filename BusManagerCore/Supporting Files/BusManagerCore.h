//
//  BusManagerCore.h
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 20/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BusManagerCore.
FOUNDATION_EXPORT double BusManagerCoreVersionNumber;

//! Project version string for BusManagerCore.
FOUNDATION_EXPORT const unsigned char BusManagerCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BusManagerCore/PublicHeader.h>


