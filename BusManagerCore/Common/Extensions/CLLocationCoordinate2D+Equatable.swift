//
//  CLLocationCoordinate2D+Equatable.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 21/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import MapKit

extension CLLocationCoordinate2D: Equatable {}

public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}
