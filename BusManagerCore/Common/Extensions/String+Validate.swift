//
//  String+Validate.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - String + Validate
extension String {
    func validate(forType type: ValidatorType) throws -> String {
        let validator = ValidatorManager.validator(forType: type)
        return try validator.validate(self)
    }
}
