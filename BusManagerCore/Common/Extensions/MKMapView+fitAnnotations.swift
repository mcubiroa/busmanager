//
//  MKMapView+fitAnnotations.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 18/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import MapKit

extension MKMapView {
    func fitRoute(for coordinates: [CLLocationCoordinate2D]) {
        var zoomRect = MKMapRect.null;
        for coordinate in coordinates {
            let annotationPoint = MKMapPoint(coordinate)
            let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.1, height: 0.1);
            zoomRect = zoomRect.union(pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 20, left: 30, bottom: 20, right: 30), animated: true)
    }
}
