//
//  String+Localized.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: "Localizable", bundle: Bundle(identifier: "marc.cubiro.BusManagerCore") ?? .main, value: "**\(self)**", comment: "")
    }
}
