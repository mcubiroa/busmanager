//
//  ValidatorManager.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - ValidationError
public struct ValidationError: Error {
    var message: String
    
    public init(message: String) {
        self.message = message
    }
}

// MARK: - ValidatorType
public enum ValidatorType {
    case email
    case phone
}

// MARK: - ValidatorType
public final class ValidatorManager {
    class func validator(forType type: ValidatorType) -> Validator {
        switch type {
        case .email:
            return EmailValidator()
        case .phone:
            return PhoneValidator()
        }
    }
}

// MARK: - EmailValidator
public final class EmailValidator: Validator {
    public func validate(_ value: String) throws -> String{
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError(message: "core_validatorError_email".localized)
            }
        } catch {
            throw ValidationError(message: "core_validatorError_email".localized)
        }
        return value
    }
}

// MARK: - PhoneValidator
public final class PhoneValidator: Validator {
    public func validate(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "(\\+34|0034|34)?[ -]*(6|7)[ -]*([0-9][ -]*){8}", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError(message: "core_validatorError_phone".localized)
            }
        } catch {
            throw ValidationError(message: "core_validatorError_phone".localized)
        }
        return value
    }
}
