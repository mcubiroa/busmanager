//
//  BadgeManager.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UserNotifications
import UIKit

// MARK: - BadgeManager
public final class BadgeManager {
    class func increaseBadget() {
        UNUserNotificationCenter.current().requestAuthorization(options: .badge) { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber += 1
                }
            }
        }
    }
}
