//
//  DateManager.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 22/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - DateFormat
enum DateFormat: String {
    case iso8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case ddMMyyyy = "dd/MM/yyyy"
}

// MARK: - DateManager
public final class DateManager {
    class func date(from string: String, withFormat format: DateFormat = .iso8601) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        return formatter.date(from: string)
    }
    
    class func string(from date: Date, format: DateFormat = .ddMMyyyy) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format.rawValue
        return formatter.string(from: date)
    }
}
