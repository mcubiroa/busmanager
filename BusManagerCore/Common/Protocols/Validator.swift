//
//  Validator.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import Foundation

// MARK: - ValidatorProtocol
public protocol Validator {
    func validate(_ value: String) throws -> String
}
