//
//  CoordinatorProtocol.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 17/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import UIKit

public protocol Coordinator: AnyObject {
    var navigationController: UINavigationController { get set }
    var coordinators: [Coordinator] { get set }
    
    func start()
}
