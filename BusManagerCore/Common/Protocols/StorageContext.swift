//
//  StorageContext.swift
//  BusManagerCore
//
//  Created by Marc Cubiro Aguilar on 23/02/2020.
//  Copyright © 2020 marc.cubiro. All rights reserved.
//

import CoreData

// MARK: StorageContext
public protocol StorageContext {
    func create<DBEntity: Storable>(_ model: DBEntity.Type) -> DBEntity?

    func save(object: Storable) throws

    func fetch(_ model: Storable.Type, predicate: NSPredicate?) -> [Storable]
}

extension StorageContext {
    func objectWithObjectId<DBEntity: Storable>(objectId: NSManagedObjectID) -> DBEntity? {
        return nil
    }
}
